# 14/10/2021
## MAN003412

### Frontend javascript
* **Dans le livrable** : 
  - Modification et montée de version de l'image docker du frontend dans le dossier `front\docker` : `agorha2-front-1.8.tar` => `agorha2-front-1.9.tar`

### Backend java
* **Dans le livrable** :
  - Modification et montée de version de l'image docker dans le dossier `backend\docker` : `agorha2-api_1.8.tar` => `agorha2-api-1.9.tar`
  - Modification du fichier `application-prod.yml` dans le dossier `backend\config`
  - Modification des fichiers json des métamodèles dans le dossier `backend\config\metamodel`

 ### Typo3
* **Dans le livrable** :
  - Modification et montée de version de l'image docker de typo3 dans le dossier `typo3\docker` : `agorha2-typo3-rec-1.6.tar` => `agorha2-typo3-prod-1.9.tar`
* **Dans le manuel** : 
  - Modification du chapitre `16.6 Personnalisation du fichier de configuration`, ajout de la configuration pour matomo
  - Ajout du chapitre `16.12 Possibles erreurs` qui liste les erreurs courantes et leur résolutions suite à l'installation de typo3

### Nginx
* **Dans le livrable** :
   - Modification du fichier de configuration pour l'environnement de recette et de prod dans le dossier `nginx\config\reverseproxy`


# 30/08/2021
## MAN003411

### Frontend javascript
* **Dans le livrable** : 
 - Modification et montée de version de l'image docker du frontend dans le dossier `front\docker` : `agorha2-front-1.7.tar` => `agorha2-front-1.8.tar`

### Backend java
* **Dans le livrable** :
 - Modification et montée de version de l'image docker dans le dossier `backend\docker` : `agorha2-api_1.7.tar` => `agorha2-api-1.8.tar`
 - Modification des fichiers `application-prod.yml` et `application-recette.yml` dans le dossier `backend\config`
 - Modification des fichiers json des métamodèles dans le dossier `backend\config\metamodel`

 ### Typo3
* **Dans le livrable** :
 - Modification et montée de version de l'image docker de typo3 dans le dossier `typo3\docker` : `agorha2-typo3-rec-1.5.tar` => `agorha2-typo3-rec-1.6.tar`
 - Modification des scripts sql dans `typo3\conf`

### Nginx
* **Dans le livrable** :
  - Modification du fichier de configuration pour l'environnement de recette et de prod dans le dossier `nginx\config\reverseproxy`

### jOAI
* **Dans le livrable** :
 - Modification et montée de version de l'image docker dans `joai\docker`, agorha2-joai_1.1.tar => agorha2-joai_1.2.tar

### Elasticsearch
* **Dans le manuel** : 
 - Modification dans le chapitre `10.3 Installation d’Elasticsearch`, déclinaison des valeurs de RAM Xms et Xmx par envrionnement

# 15/06/2021
## MAN003410

### Frontend javascript
* **Dans le livrable** : 
 - Modification et montée de version de l'image docker du frontend dans le dossier `front\docker` : `agorha2-front-1.5.tar` => `agorha2-front-1.7.tar`

### Backend java
* **Dans le livrable** :
 - Modification et montée de version de l'image docker dans le dossier `backend\docker` : `agorha2-api_1.6.tar` => `agorha2-api-1.7.tar`
 - Modification des fichiers `application-prod.yml` et `application-recette.yml` dans le dossier `backend\config`
 - Modification des fichiers json des métamodèles dans le dossier `backend\config\metamodel`

 ### Typo3
* **Dans le livrable** :
 - Modification et montée de version de l'image docker de typo3 dans le dossier `typo3\docker` : `agorha2-typo3-rec-1.4.tar` => `agorha2-typo3-rec-1.5.tar`

### Talend
* **Dans le livrable** : 
 - Modifications des fichiers  dans `talend\jobs`


# Sprint 6
## MAN003410

### Frontend javascript
* **Dans le livrable** : 
 - Modification et montée de version de l'image docker du frontend dans le dossier `front\docker` : `agorha2-front-1.4.tar` => `agorha2-front-1.5.tar`

### Nginx
* **Dans le livrable** :
 - Ajout de fichiers de configuration pour l'exposition des thesaurus sur un sous-domaine spécifique dans le dossier `nginx\config\reverseproxy` (thesaurus-prod.conf, et thesaurus-recette.conf)
  - Modification du fichier de configuration pour l'environnement de recette et de prod dans le dossier `nginx\config\reverseproxy`, modification de la manière de réécrire les url pour les media et les notice (ark:/), ajout d'une configuration pour les url ark des BDD, augmentation timeout sur les requêtes à ginco
* **Dans le manuel** :
 - Modification chapitre `17.3 Procédure d’installation `, prise en compte des nouveaux fichiers de configuration

### Backend java
* **Dans le livrable** :
 - Modification et montée de version de l'image docker dans le dossier `backend\docker` : `agorha2-api_1.5.tar` => `agorha2-api-1.6.tar`
 - Modification des fichiers `application-prod.yml` et `application-recette.yml` dans le dossier `backend\config`
 - Modification des fichiers json des métamodèles dans le dossier `backend\config\metamodel`
* **Dans le manuel** : 
 - Modification dans le chapitre `21.4` dans la commande docker :
   - "-v /opt/log/api:/usr/local/agorha2/log \" => "-v /opt/log/api:/usr/local/agorha2/**server**/log \"
   - "-v /opt/app/api/batch:/usr/local/agorha2/batch \" => "-v /opt/app/api/batch:/usr/local/agorha2/**server**/batch \"
   - "-v /opt/app/api/config:/usr/local/agorha2/config \" => "-v /opt/app/api/config:/usr/local/agorha2/**server**/config \"
   - "-e SPRING_OPTIONS=-Dspring.config.additional-location=file:/usr/local/agorha2/config/application-agorha.yml \ => "-e SPRING_OPTIONS=-Dspring.config.additional-location=file:/usr/local/agorha2/**server**/config/application-agorha.yml \"
 - Ajout instructions dans le chapitre `21.4` : "Créer les répertoires pour l’import CSV de notices et l’import Json de médias :
 `sudo mkdir –p /opt/app/api/batch/importCsv`
 `sudo mkdir –p /opt/app/api/batch/media/json`
 `sudo mkdir –p /opt/app/api/batch/media/resources`"
 - Ajout instructions dans le chapitre `21.4` : "Créer le répertoire qui contient les données OAI exposées
 `cd /var/lib/docker/volumes/joai-data-provider/_data && sudo mkdir agorha2`
Changer le propriétaire du volume docker pour les données OAI 
`cd /var/lib/docker/volumes/joai-data-provider/_data && sudo chown -R agorha:agorha agorha2`"

### Jenkins
* **Dans le livrable** : 
 - Modification des jobs dans le dossier `jenkins\config\jobs`, et ajout du jobs create_attributes_th_lieu
 - Ajout des fichiers config-recette.xml et config-prod.xml dans le dossier `jenkins\config`
* **Dans le manuel** : 
 - Ajout instructions dans le chapitre `18.2` : "Remplacer le fichier /opt/app/jenkins/config.xml par le fichier du livrable /jenkins/config/config-recette.xml (ou config-prod.xml suivant l’environnement)"
 - Ajout chapitre `23.1.1 Modifier le mot de passe du compte batch dans jenkins`

### jOAI
* **Dans le livrable** :
 - Modification et montée de version de l'image docker dans `joai\docker`, agorha2-joai_1.0.tar => agorha2-joai_1.1.tar
 - Ajout du dossier `joai\conf` qui contient le fichier de paramétrage des utilisateurs tomcat pour activer l'authentification sur l'administration joai
* **Dans le manuel** : 
 - Ajout instructions dans le chapitre `13.2 Installation` : "Créer le répertoire contenant la configuration de tomcat  
`sudo mkdir -p /opt/app/joai/tomcat/conf`  
Modifier et déplacer dans ce nouveau répertoire le fichier tomcat-users.xml contenu dans le répertoire /joai/conf du livrable  
Modifier les <ADMIN_JOAI_USER> et <ADMIN_JOAI_PASSWD> dans la ligne suivante  
`<user username="<ADMIN_JOAI_USER>" password="<ADMIN_JOAI_PASSWD>" roles="oai_admin"/>`"  
 - Modification ligne de commande pour lancement application dans le chapitre `13.2 Installation`, ajout d'un volume dans la commande docker `-v /opt/app/joai/tomcat/conf/tomcat-users.xml:/usr/local/tomcat/conf/tomcat-users.xml \`

### Typo3
* **Dans le livrable** :
 - Modification du fichier AdditionnalConfiguration.php, ajout du user admin pour l'api backend et de la conf pour exposition derrière un reverse proxy
 - Remplacement du fichier .sql.zip par deux fichier .sql (1 par envrionnement) dans `typo3\conf`, qu'il faut paramétrer avant utilisation
 - Ajout dossier `typo3\conf\autoload` et de 3 fichiers à l'intérieur
* **Dans le manuel** : 
 - Modification chapitre `16.5 Installation des extensions non contenues dans l’image`, prise en compte des nouveaux fichiers autoload
 - Modification chapitre `16.6 Personnalisation du fichier de configuration`, ajout paragraphe `Copier le fichier AdditionalConfiguration.php contenu dans le dossier typo3/config du livrable dans le dossier /var/lib/docker/volumes/docker_typo3conf/_data sur le serveur (il faut bien remplacer le fichier déjà existant).`
 - Modification chapitre `16.6 Personnalisation du fichier de configuration`, prise en compte des nouveautés dans le fichier de config
 - Ajout d'un chapitre `23.2 Configuration extension mksearch pour typo3` pour paramétrer l'extension mksearch
 - Ajout d'un chapitre `16.10 Désactiver l’indexation du site par Google` à ne prendre en compte que pour l'environnement de production
 - Ajout d'un chapitre `16.11 Mise en place du scheduler`
 - Découpage du chapitre `16.7 Alimentation initiale de la base de données de typo3` en deux sous-chapitre pour prendre en compte la modification du fichier .sql.zip du livrable

### Général
* **Dans le manuel** :
 - Modification chapitre `23.1 Modifications du mot de passe des comptes de service`, ajout rappel pour repporter le changement de mdp dans la conf de typo3, ajout 
 - Modification chapitre `21.6 Création des utilisateurs système`, ajout création utilisateur batch

### Talend
* **Dans le livrable** : 
 - Modifications des fichiers  dans `talend\working_directory\input`, suppression des anciens .csv et ajout de FileGeonames.csv
 - Modifications des fichiers  csv dans `talend\working_directory\input\thesaurus`
 - Ajout des fichiers d'import du thesaurus des sicèles dans le dossier `talend\working_directory\input\thesaurus`, TH_SIECLES_GINCO_FORMAT_PROD.xml et TH_SIECLES_GINCO_FORMAT_RECETTE.xml

### Kibana
* **Dans le livrable** :
 - Ajout fichier dans `kibana\conf`
* **Dans le manuel** : 
 - Ajout chapitre `10.8 Chargement de la configuration initiale de Kibana` qui permet de charger la configuration de base de Kibana

### Ginco
* **Dans le manuel** : 
 - Dans les chapitres  `19.3.1 Paramètres à définir` et `19.3.2 Installation de PostgreSQL`, ajout précision sur l'impact du choix du login administrateur de ginco

### Hymir
* **Dans le manuel** : 
 - Dans le chapitres  `14.3 Installation`, ajout de l'instruction "Copier le fichier rules.yml du répertoire /hymir/config du livrable dans le répertoire /opt/app/hymir/config"

### Openldap
* **Dans le livrable** :
 - Modification du fichier `50-bootstrap.ldif` pour ajouter l'utilisateur batch du site

## MAN00348
### Jenkins
* **Dans le livrable** :
 - Ajout de nouveaux jobs dans le dossier `jenkins/config/jobs` (Export_OAI, Import CSV, Launch_Ebooks, Import Media) 

### Typo3
* **Dans le livrable** :
 - Ajout de deux archives contenant des extensions
 - Modification et montée de version de l'image docker de typo3 dans le dossier `typo3\docker` : `agorha2-typo3-rec-1.2.tar` => `agorha2-typo3-rec-1.3.tar`
* **Dans le manuel** :
 - Ajout chapitre `16.3 Nettoyage ancienne installation` pour nettoyer ancienne installation de typo3
 - Ajout chapitre `16.5 Installation des extensions non contenues dans l’image` pour installation des nouvelles extensions dans le package
 - Modification chapitre `16.4 Installation`, ajout paragraphe `Installer le plugin ldap pour php`
 
### Frontend javascript
* **Dans le livrable** : 
 - Modification et montée de version de l'image docker du frontend dans le dossier `front\docker` : `agorha2-front-rec-1.3.tar` => `agorha2-front-1.4.tar`
* **Dans le manuel** : 
 - Suppression paragraphe sur la non possibilité de variabiliser l'environnement au lancement de l'application dans le chapitre `15.3 Installation`
 - Modification ligne de commande pour lancement application dans le chapitre `15.3 Installation`, ajout des paramètres d'environnement

### Backend java
* **Dans le livrable** :
 - Montée de version de l'image docker du backend dans le dossier `backend\docker` : `agorha2-api_1.4.tar` => `agorha2-api_1.5.tar`
 - Modification des fichiers `application-prod.yml` et `application-recette.yml` dans le dossier `backend\config`
 - Modification des fichiers json des métamodèles dans le dossier `backend\config\metamodel`
 - Ajout ligne dans le chapitre `21.4` dans la commande docker : "-v joai-data-provider:/usr/local/agorha2/server/joai/provider\"

### Nginx
* **Dans le livrable** :
 - Modification du fichier de configuration pour l'environnement de recette et de prod dans le dossier `nginx\config\reverseproxy`, remplacement de http par https dans le contenu html renvoyé par ginco-diff
 - Modification du fichier de configuration général  dans le dossier `nginx\config\reverseproxy` common.conf

# Sprint 5
## MAN00347
### Nginx
* **Dans le livrable** :
 - Modification du fichier de configuration pour l'environnement de recette (ligne 153 "proxy_pass http://agorhatest.inha.fr/;" => "proxy_pass http://agorhatest.inha.fr:8083/;"

### Elasticsearch
* **Dans le manuel** : 
 - Ajout désactivation du swap au chapitre `10.2`

### Ginco
* **Dans le manuel** : 
 - Inversion des chapitres `19.3.4 Installation de la JVM` et `19.3.3 Installation de Apache Solr`

## MAN00347
### Openldap
* **Dans le livrable** :
 - Modification du fichier `50-bootstrap.ldif` pour ajouter l'utilisateur admin du site

### Nginx
* **Dans le livrable** :
 - Modification et montée de version de l'image docker dans le dossier `nginx\docker` : `agorha2-nginx-1.1.tar` => `agorha2-nginx-1.2.tar`
* **Dans le manuel** : 
 - Suppression de la partie spécifique à l'environnement de recette dans le chapitre `4.2` 
 - Ajout instruction dans le chapitre `17.3` : "Pour la plateforme de recette seulement, modifier le fichier nginx-rec.conf pour la location « / » en ajoutant le port choisi (8083) à l’url d’accès à typo3 (comme dans l’exemple ci-dessous)"

### Elasticsearch
* **Dans le manuel** : 
 - Ajout chapitre `10.5` pour la création de l'utilisateur admin du site

### Ginco-diff
* **Dans le livrable** : 
 - Modification de l'image docker `agorha2-ginco-diff_1.0.tar` dans le dossier `ginco-diff\docker` pour lui assigner un tag de version correct (1.0 à la place de lateset)

### Jenkins
* **Dans le livrable** : 
 - Modification des jobs dans le dossier `jenkins\config\jobs`

### Talend
* **Dans le livrable** : 
 - Modification des jobs dans le dossier `talend\jobs`

## MAN00346
### Elasticsearch
* **Dans le livrable** : 
 - Ajout du fichier `stopwords.lst` dans le dossier `elasticsearch\config`
* **Dans le manuel** : 
 - Ajout instruction dans le chapitre `10.2` : "Copier le fichier /elasticsearch/config/stopwords.lst dans le répertoire /opt/app/elasticsearch/config de chaque serveur Elasticsearch."
 - Ajout ligne dans le chapitre `10.3` dans la commande docker : "-v /opt/app/elasticsearch/config/stopwords.lst:/usr/share/elasticsearch/config/stopwords.lst \"

### Frontend javascript
* **Dans le livrable** : 
 - Modification et montée de version de l'image docker du frontend dans le dossier `front\docker` : `agorha2-front-rec-1.0.tar` => `agorha2-front-rec-1.1.tar`

### Typo3
* **Dans le livrable** : 
 - Modification de l'image docker de typo3 dans le dossier `typo3\docker` : `agorha2-typo3-rec-1.1.tar`
 - Modification de la configuration de typo3 dans le dossier `typo3\config` : `agorha2-25-08-2020-release-sprint5.sql.zip`
 - Modification du fichier `docker-compose.yml` dans le dossier `typo3\docker`

### Jenkins 
* **Dans le manuel** : 
 - Ajout ligne dans le chapitre `18.2` dans la commande docker : "-v joai-data:/app/joai \"
 
### Ginco
* **Dans le livrable** : 
 - Modification du fichier `ginco.properties` dans les dossiers `ginco\config\recette` et `ginco\config\prod`
* **Dans le manuel** : 
 - Ajout instruction dans le chapitre `19.3.5` : "Créer le répertoire qui contiendra les fichiers de publication de thesaurus :
sudo mkdir -p /opt/app/ginco-diff/publish"

### Backend java
* **Dans le livrable** :
 - Ajout du dossier `metamodel` dans le dossier `backend\config`, contenant les fichiers `schemaArtwork.json` `schemaCollection.json` `schemaEvent.json` `schemaPerson.json` `schemaReference.json`
 - Montée de version de l'image docker du backend dans le dossier `backend\docker` : `agorha2-api_1.1.tar` => `agorha2-api_1.4.tar`
 - Modification des fichiers `application-prod.yml` et `application-recette.yml` dans le dossier `backend\config`
* **Dans le manuel** : 
 - Modification instruction dans le chapitre `21.4` : "Copier le fichier contenu dans le livrable /backend/config dans le répertoire config nouvellement créé" => "Copier le contenu du répertoire du livrable /backend/config dans le répertoire config nouvellement créé"
 - Ajout ligne dans le chapitre `21.4` dans la commande docker : "-v /opt/files/manifest:/app/agorha2/manifest \"

### Ginco-diff
* **Dans le livrable** : 
 - Modification de l'image docker `agorha2-ginco-diff_1.0.tar` dans le dossier `ginco-diff\docker`