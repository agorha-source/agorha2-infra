#!/bin/bash
docker stop api|| true && docker rm api || true &&\
docker run -d \
--name api \
--user 1001:1001 \
-e JAVA_OPTIONS="-Xmx2G -Xmx4G -Djavax.net.ssl.trustStore=/usr/local/agorha2/server/truststore/cacerts -Djavax.net.ssl.trustStorePassword=Y8MVRrk2xzRv!3c6m^Jt -Dlog4j2.formatMsgNoLookups=true" \
-e SPRING_OPTIONS="-Dspring.profiles.active=rec" \
-e TZ=CET \
-p 8080:8080 \
-v /opt/log/api:/usr/local/agorha2/server/log \
-v /opt/app/api/batch:/usr/local/agorha2/server/batch \
-v /opt/app/api/config:/usr/local/agorha2/server/config \
-v /opt/files/media:/app/agorha2/media \
-v /opt/files/manifest:/app/agorha2/manifest \
-v joai-data-provider:/usr/local/agorha2/server/joai/provider \
-v /opt/app/java/truststore:/usr/local/agorha2/server/truststore \
--restart=on-failure:3 \
agorha2/api:$1
