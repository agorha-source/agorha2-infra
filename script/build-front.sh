#!/bin/bash

cd /platform/git/agorha2-front && \
git fetch &&\
git checkout release/$1 || git checkout feat/$1 || git checkout fix/$1 || git checkout $1 &&\
git pull &&\
cd app &&\
docker build --no-cache -t agorha2/front:$1 .
