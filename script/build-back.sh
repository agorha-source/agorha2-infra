#!/bin/bash

cd /platform/git/agorha2 && \
git fetch &&\
git checkout release/$1 || git checkout feat/$1 || git checkout fix/$1 || git checkout $1 &&\
git pull &&\
mvn clean install -pl agorha2-elastic,agorha2-server -DskipTests -Pjar -Dmaven.compiler.fork=true -Dmaven.compiler.executable=/usr/lib/jvm/java-11-openjdk-11.0.18.0.10-1.el7_9.x86_64/bin/javac &&\
cd agorha2-server/ &&\
docker image rm agorha2/api || true &&\
docker build -t agorha2/api:$1 . &&\
docker save agorha2/api:$1 > /tmp/agorha2-api-$1.tar
