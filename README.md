# agorha2-infra

**Ce projet contient :**
 * Les fichiers de configuration quand ceux-ci ne sont pas rattachés à un serveur  
 * Les dockerFiles des images docker quand ceux-ci ne sont pas rattaché à du code source 
 
# Création du livrable pour l'INHA

TODO décrire pour chaque composant
**Points d'attention :**
 * URLs utilisées dans l'image front
 * URLs utilisées dans l'image typo3 (public\typo3conf\ext\agorha_base\Build\Vuidget\src\config\api-config.js et public\typo3conf\AdditionalConfiguration.php)
 * URLs dans les fichier de configuration de nginx
 * Modifier les URLs dans les jobs jenkins "Batch_post_reprise", "Export_OAI", "Import CSV" et "Import Media"
 * Vérifier que les jobs talend soient bien buildés pour l'environnement ciblé (fichier jobInfo.properties)
 * Vérifier les infos pour mksearch et ldap dans le sql d'init de typo3