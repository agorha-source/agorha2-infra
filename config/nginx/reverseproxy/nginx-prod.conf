server {
	listen 80 default_server;
	listen [::]:80 default_server;
	server_name agorha.inha.fr;
	return 301 https://$host$request_uri;
}

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;
	server_tokens off;
	server_name agorha.inha.fr;
	ssl_certificate /etc/nginx/ssl/cert.crt;
	ssl_certificate_key /etc/nginx/ssl/cert.key;

    location /health {
    	return 200 'UP';
    }

	location /joai {
		proxy_pass http://agopbackend01.inha.fr:8082/joai;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 5M;
	}
	location /kibana/ {
		proxy_pass http://agopsearch01.inha.fr:5601/kibana/;
		rewrite ^/kibana/(.*)$ /$1 break;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
		# bug avec les cookies dans kibana
		proxy_hide_header Set-Cookie;
		proxy_ignore_headers Set-Cookie;
		proxy_set_header Cookie "";
	}
	location /agorha2-api {
		proxy_pass http://agopbackend01.inha.fr:8080/api;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}
	location /api {
		proxy_pass http://agopbackend01.inha.fr:8080/api;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}

	location /nuxt/ {
		proxy_pass http://agopfrontend01.inha.fr:3000/;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}
	location /_nuxt {
		proxy_pass http://agopfrontend01.inha.fr:3000;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}

	location ~* ^/ark:/54721/([^/]+)/doc/([^/]+)/(.*) {
		proxy_pass http://agopbackend01.inha.fr;
		rewrite ^/ark:/54721/([^/]+)/doc/([^/]+)/(.*)$ /media/$1/$2/$3 break;
		proxy_http_version 1.1;
		proxy_redirect off;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 5M;
	}

	location ~* ^/ark:/54721/([0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+)$ {
		proxy_pass http://agopfrontend01.inha.fr;
		rewrite ^/ark:/54721/([0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+)$ /recherche?tx_agorhanuxtjs_search%5Baction%5D=displayNotice&tx_agorhanuxtjs_search%5Bcontroller%5D=Search&uuid=$1&database=$arg_database break;
		proxy_redirect off;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}
	location ~* ^/ark:/54721/([0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+)\.json$ {
			proxy_pass http://agopbackend01.inha.fr:8080/api/notice/$1;
			proxy_redirect off;
			proxy_http_version 1.1;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header Accept application/json;
			proxy_max_temp_file_size 0;
	}
	location ~* ^/ark:/54721/([0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+)\.jsonld$ {
			proxy_pass http://agopbackend01.inha.fr:8080/api/notice/$1;
			proxy_redirect off;
			proxy_http_version 1.1;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header Accept application/ld+json;
			proxy_max_temp_file_size 0;
	}
	location ~* ^/ark:/54721/([0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+)\.rdf$ {
			proxy_pass http://agopbackend01.inha.fr:8080/api/notice/$1;
			proxy_redirect off;
			proxy_http_version 1.1;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header Accept application/rdf+xml;
			proxy_max_temp_file_size 0;
	}
	location ~* ^/ark:/54721/([0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+)\.n3$ {
			proxy_pass http://agopbackend01.inha.fr:8080/api/notice/$1;
			proxy_redirect off;
			proxy_http_version 1.1;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header Accept text/rdf+n3;
			proxy_max_temp_file_size 0;
	}
	location ~* ^/ark:/54721/([0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+)\.n4$ {
			proxy_pass http://agopbackend01.inha.fr:8080/api/notice/$1;
			proxy_redirect off;
			proxy_http_version 1.1;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header Accept application/n-triples;
			proxy_max_temp_file_size 0;
	}
	location ~* ^/ark:/54721/([0-9]+)$ {
		proxy_pass http://agopfrontend01.inha.fr;
		rewrite ^/ark:/54721/([0-9]+)$ /database/$1 break;
		proxy_redirect off;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}

	location ~* ^/inhaprod/ark:/54721/(0[01][0-9])([0-9]+)$ {
		proxy_pass http://agopbackend01.inha.fr:8080/api/notice/redirect?id=$2&type=$1;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}

	location /ginco-admin {
		proxy_pass http://agopginco01.inha.fr:8080;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
		proxy_read_timeout 14400;
		proxy_connect_timeout 14400;
		proxy_send_timeout 14400;
	}

	location /thesaurus {
		# Pour un contenu zippé avec sub_filter : https://www.nginx.com/resources/wiki/modules/substitutions/
		proxy_set_header Accept-Encoding "";
		proxy_pass http://agopginco01.inha.fr:8085;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
		client_max_body_size 0;
		# Pour modifier les url non sécurisées retournées par ginco-diff sur ses ressources
		sub_filter 'http://' 'https://';
		sub_filter_once off;
	}
	location /iiif/presentation {
		proxy_pass http://agopbackend01.inha.fr:8081/iiif/presentation;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}

	location /iiif/image {
		proxy_pass http://agopbackend01.inha.fr:8081/iiif/image;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}

	location / {
		proxy_pass http://agopfrontend01.inha.fr/;
		proxy_http_version 1.1;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_max_temp_file_size 0;
	}
}
