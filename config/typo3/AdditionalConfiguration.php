<?php

$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'] = array_merge($GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'], [
    'dbname' => 'typo3',
    'host' => 'typo3_db',
    'password' => '<MARIA_DB_PASSWORD>',
    'port' => '3306',
    'user' => '<MARIA_DB_LOGIN>',
]);

$GLOBALS['NUXT_URL'] = 'https://<DOMAIN_NAME>/nuxt/';
$GLOBALS['AGORHA2_API_URL'] = 'https://<DOMAIN_NAME>/agorha2-api';
$GLOBALS['COOKIE_DOMAIN'] = '<DOMAIN_NAME>';
$GLOBALS['RECHERCHE_URL'] = '/recherche?terms=&page=1&pageSize=20&sort=&fieldSort=&noticeType=';

// Utile pour le tracking matomo
$GLOBALS['MATOMO_URL'] = 'https://<DOMAIN_NAME>/matomo/';
$GLOBALS['MATOMO_ID'] = '<MATOMO_CONTAINER_ID>';

// Utile pour la connexion utilisateur sur l'indexation des news
$GLOBALS['USER_ADMIN_MAIL'] = 'admin@mail.com';
$GLOBALS['USER_ADMIN_PASSWORD'] = '<API_ADMIN_PASSWORD>';

// Configuration pour fonctionner derrière un reverse proxy en https
$GLOBALS['TYPO3_CONF_VARS']['SYS']['reverseProxyIP'] = '*';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['reverseProxyHeaderMultiValue'] = 'last';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['reverseProxySSL'] = '*';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['trustedHostsPattern'] = '<DOMAIN_NAME>';
